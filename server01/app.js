var express = require('express');
var http = require('http');
var Mongoose = require('mongoose');
var db = Mongoose.createConnection('localhost', 'server01');

var app = express();

app.configure(function(){
	app.set('port', process.env.PORT || 3000);

});

app.get('/', function(req, res){

	res.send('This file was edited in local eclipse.');
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
